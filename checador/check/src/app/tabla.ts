export interface Tabla {
  empleado_id: number;
  nombre: string;
  apellidoP: string;
  apellidoM: string;
  clave: string;
  imagen: string;
}
