import { Component, OnInit } from '@angular/core';
import { TrabajadoresService } from '../tab1/providers/trabajadores.service';
//import { Simonsito } from '../simonsito'
import {Tabla} from '../tabla'
import { Asist } from '../asist';
import { Crossref } from '../crossref';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})

export class Tab1Page implements OnInit{
  usuarios: Crossref []=[];
  asistencias: Asist []=[];
  cruzada: Crossref []=[];
  simon: boolean = false;
  color: String="light";
  
  constructor(public Trabajadores: TrabajadoresService) {
    this.Trabajadores.empleados()
      .subscribe(
        (res:any)=>{
          this.usuarios=res;
        }
      )
      console.log(this.usuarios);
      this.Trabajadores.asistencias()
      .subscribe(
        (ponse:any)=>{
          this.asistencias=ponse
        }
      )
      console.log(this.asistencias);  


  }
  ngOnInit():void{
   
  }
  ngDoCheck(){
    var temp: Crossref []=[];

    this.usuarios.forEach(element => {
      this.asistencias.forEach(item => {
        if(element.empleado_id===item.empleado_id){
          if(item.asistio==1){
            element["color"]="success";
          }
          else{
            if(item.asistio==2){
              element["color"]="warning";
            }
            else{
              if(item.asistio==3){
                element["color"]="danger";
              }
              else{
                element["color"]="light";
              }
            }
          }
          element["asist"]=item.asistio;
          temp.push(element);
        }
      });
    });
    console.log(temp);
    this.cruzada=temp;
    console.log(this.cruzada);
  }
}