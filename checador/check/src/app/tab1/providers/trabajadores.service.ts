import { Injectable } from '@angular/core';
import{HttpClientModule,HttpClient}from'@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TrabajadoresService {

  constructor(public http: HttpClient) { }

  empleados(){
    return this.http.get('http://192.168.0.18:3001/api/empleados/');
    
  }
  asistencias(){
    return this.http.get('http://192.168.0.18:3001/api/asistencias/');
  }
  deleteEmploye(id){
    return this.http.delete('http://192.168.0.18:3001/api/empleados/for/empleado_id/'+id).subscribe(
      ()=>console.log(`Empleado ${id} borrado`),
      (err)=>console.log(err)
    );
  }
  createEmploye(nombre,apellidoP,apellidoM,imagen){
    return this.http.post('http://192.168.0.18:3001/api/empleados/',
    {"nombre":nombre,"apellidoP":apellidoP,"apellidoM":apellidoM,"imagen":imagen}).subscribe(
      ()=>console.log('empleado añadido'),
      (err)=>console.log(err)
    );
  }
  createAsist(id){
    return this.http.post('http://192.168.0.18:3001/api/asistencias/',
    {"empleado_id":id}).subscribe(
      (res)=>console.log(res),
      (err)=>console.log(err)
    );
  }
  editEmploye(id,value,type){
    return this.http.put('http://192.168.0.18:3001/api/empleados/for/'+id+'/'+type+'/'+value,{}).subscribe(
      (res)=>console.log(res),
      (err)=>console.log(err)
    );
  }
}
