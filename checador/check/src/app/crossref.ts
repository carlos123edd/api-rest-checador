export interface Crossref {
    empleado_id: number;
    nombre: string;
    apellidoP: string;
    apellidoM: string;
    clave: string;
    imagen: string;
    assist: boolean;
    color: string;
  }
