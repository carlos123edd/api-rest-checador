import { Component } from '@angular/core';
import { Tabla } from '../tabla';
import { TrabajadoresService } from '../tab1/providers/trabajadores.service';
import { ActionSheetController } from '@ionic/angular';


@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {
usuarios: Tabla[]=[];


  constructor(public Trabajadores:TrabajadoresService,public actionSheetController: ActionSheetController) {
    this.Trabajadores.empleados()
    .subscribe(
      (res:any)=>{
        this.usuarios=res
      }
    )
    console.log(this.usuarios); 
  }
  
  async presentActionSheet(i,nombre,paterno,materno,imagen) {
    const actionSheet = await this.actionSheetController.create({
      header: 'Editar',
      buttons: [{
        text: 'Nombre',
        handler: () => {
          this.openModalName(i,nombre)
        }
      }, {
        text: 'Apellido Paterno',
        handler: () => {
          this.openModalPaterno(i,paterno)
        }
      }, {
        text: 'Apellido MAterno',
        handler: () => {
          this.openModalMaterno(i,materno)
        }
      }
      ]
    });
    await actionSheet.present();
  }

  openOptions(){

  }

  openModalName(i,nombre){
    var name = prompt("Nombre:", nombre);
    this.Trabajadores.editEmploye(i,name,"nombre");
    setTimeout(function(){window.location.reload()},1500)
  }
  openModalPaterno(i,paterno){
    var name = prompt("Apellido Paterno:", paterno);
    this.Trabajadores.editEmploye(i,name,"apellidoP");
    setTimeout(function(){window.location.reload()},1500)
  }
  openModalMaterno(i,materno){
    var name = prompt("Apellido Materno:", materno);
    this.Trabajadores.editEmploye(i,name,"apellidoM");
    setTimeout(function(){window.location.reload()},1500)
  }

  delete(id){
    this.Trabajadores.deleteEmploye(id);
    console.log("borrale "+id);
    setTimeout(function(){window.location.reload()},1500)
  }

}
