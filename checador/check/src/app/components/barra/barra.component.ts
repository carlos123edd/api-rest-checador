import { Component} from '@angular/core';


@Component({
  selector: 'app-barra',
  templateUrl: './barra.component.html',
  styleUrls: ['./barra.component.scss'],
})
export class BarraComponent{

 public barChartOptions: any ={
  scaleSwhowVerticalLines: false,
  responsive: true
 };

 public barChartLabels: string[]=['50','100','200',]
 public barChartType: string = 'bar';
 public barChartLegend:boolean = true;

 public barChartData: any[] =[
   {data: [65,59, 81, 56, 55, 40], label: 'asistieron'},
   {data: [71,49, 51, 53, 55, 60], label: 'no asistieron'}
 ];

 public chartClicked(e:any):void{
   console.log(e);
 }

 public chartHovered(e:any):void{
   console.log(e);
 }

 public randomize():void{
   let data =[
     Math.round(Math.random()* 100),
     59,
     80,
     (Math.random()*100),
     56,
     (Math.random()*100),
     40
   ];
   let clone = JSON.parse(JSON.stringify(this.barChartData));
   clone[0].data = data;
   this.barChartData = clone;
 }
}
