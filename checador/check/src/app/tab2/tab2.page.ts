import { Component } from '@angular/core';
import { TrabajadoresService } from '../tab1/providers/trabajadores.service';
@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  constructor(public Trabajadores:TrabajadoresService) {

  }
  onSubmit(nombre,paterno,materno,img){
    this.Trabajadores.createEmploye(nombre,paterno,materno,img);
    setTimeout(function(){window.location.reload()},1500);
  }
  
}
