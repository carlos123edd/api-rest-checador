-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 28-03-2020 a las 02:20:58
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `checador`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleados`
--

CREATE TABLE `empleados` (
  `empleado_id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `apellidoP` varchar(50) DEFAULT NULL,
  `apellidoM` varchar(50) DEFAULT NULL,
  `clave` varchar(50) DEFAULT NULL,
  `imagen` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `empleados`
--

INSERT INTO `empleados` (`empleado_id`, `nombre`, `apellidoP`, `apellidoM`, `clave`, `imagen`) VALUES
(8, 'Nathalie Aide', 'Herrera', 'Barrios', '1as9c0lRy5', 'https://scontent.fdgo1-1.fna.fbcdn.net/v/t1.0-9/39580480_1391497687651120_8861491217913872384_n.jpg?_nc_cat=106&_nc_sid=85a577&_nc_ohc=fslD5q1jKfsAX9DevCt&_nc_ht=scontent.fdgo1-1.fna&oh=1e4a4e672474b272f1846c9fc4123ba3&oe=5EA4E6E0'),
(13, 'Antonio de Jesús', 'Campos', 'Avila', 'Atsf1w896q', 'https://scontent.fdgo1-1.fna.fbcdn.net/v/t1.0-9/77096275_10212683545762245_5997905336189583360_n.jpg?_nc_cat=102&_nc_sid=dd7718&_nc_ohc=7HqS0MinlmUAX_kUcKT&_nc_ht=scontent.fdgo1-1.fna&oh=91ec4cdda26971b12b0a76fd724c9075&oe=5EA32DFE'),
(15, 'Carlos Eduardo', 'Carrete', 'Rodriguez', 'rSqm2vHy67', 'https://scontent.fdgo1-1.fna.fbcdn.net/v/t1.0-9/69227054_2401318353238675_8330960223103614976_n.jpg?_nc_cat=105&_nc_sid=85a577&_nc_eui2=AeHf8xNHu0Aav0QAE-aV0spWTbp4U-apZX-7cfNm7j6iSnpKpbJSAlEBHmC9st09O7Gpp6PXc6fWj4eBWaF2v56vCKRf5ns0mjYv-x2-TVr_Ww&_nc_ohc=HrEQOK5B4v4AX82ihgY&_nc_ht=scontent.fdgo1-1.fna&oh=a54744e4a501a6df701f9587a195ba72&oe=5EA3531A'),
(27, 'Jesus Alejandro', 'Santos', 'Flores', 'iouCdwbDk8', 'https://scontent.fdgo1-1.fna.fbcdn.net/v/t1.0-9/33064458_1590851287679455_6697047225098305536_n.jpg?_nc_cat=111&_nc_sid=85a577&_nc_eui2=AeGOSNM2CjeKUaI7mYTBSp9x5XPO0rUJNZ28uuugCGm46HLNvKhFxwftNw-KlFsYla70fMFmFr5O8d7GWeECmCR-mK-TKHPCA02VHnA_HZAM6g&_nc_ohc=4ZKZKfUOiVAAX9Dt64k&_nc_ht=scontent.fdgo1-1.fna&oh=1eabbe4181653320ad7c251a3d620891&oe=5EA25C1B'),
(28, 'Andreé Fernando', 'Galvan', 'Candia', 'bbL2ZhD2kD', 'https://scontent.fdgo1-1.fna.fbcdn.net/v/t1.15752-9/91005059_2416342888588111_4472790302242897920_n.png?_nc_cat=105&_nc_sid=b96e70&_nc_eui2=AeG1TN4L2g8VDeM5Xqvd5tz7NPt9c_u7PNoe8gxVULurfftHFIZj-mveHItM2FvnmtZNr-V7rWJbKc-Rz7LoOe8-IJP9rNLdCdWe0UZrCH0TWA&_nc_ohc=aEqC2jWv4TYAX_p4Ygy&_nc_ht=scontent.fdgo1-1.fna&oh=e4a872bb68deecd563b4947120f779f1&oe=5EA2E042'),
(29, 'Luis Manuel', 'Carrillo', 'Ramirez', 'F2ySgoDYLV', 'https://scontent.fdgo1-1.fna.fbcdn.net/v/t1.0-9/29790822_1898772290154707_4439453294432054655_n.jpg?_nc_cat=100&_nc_sid=7aed08&_nc_eui2=AeHhOMtIlx3BecjPBw8DtXiStrXfhHxa-fBma8aVy95SVGxi-UD-3GY3SRZilq9QnKcU4oUuwwru5d17ORU5btuCPuvrUBjqAR9iXEbloGwfbQ&_nc_ohc=xlY6RFp3bLIAX_IMkwY&_nc_ht=scontent.fdgo1-1.fna&oh=0f5c7d114cfb02c6bfb2f0241a6373de&oe=5EA436B3'),
(30, 'Christian Chantal', 'Moreno', 'Rodriguez', 'z6ggaAuuBO', 'https://scontent.fdgo1-1.fna.fbcdn.net/v/t1.0-9/49107912_2042306085862269_3672405679001305088_n.jpg?_nc_cat=105&_nc_sid=85a577&_nc_eui2=AeHi_qJ6l3Vj8sIs228ltoSsst_KnkEPBUco7I5mTDeu1efdr75o6Vjj6-jbz2fpsu66bUpmisRuecPsv_rpVk2c9EzkT7dwDhN8SkHpUWPNvg&_nc_ohc=YTOklqzo3XgAX8xycJ6&_nc_ht=scontent.fdgo1-1.fna&oh=1a44ac6f030de6bc49dd8a4c7bb1419f&oe=5EA5E5F4'),
(31, 'Jose Manuel', 'Valdez', 'Gonzales', '6lH28sYltP', 'https://scontent.fdgo1-1.fna.fbcdn.net/v/t1.0-9/19894707_1529559363760948_4577187797770333344_n.jpg?_nc_cat=103&_nc_sid=13bebb&_nc_eui2=AeE4NIXhjOeZ_879HxaPM8t75bROqftqo3MZafc0wV4EcUyC33ynHxUQSJNRadViXgGeJpdHSfumx2siyliqelHNpRZ4xqhG9AYWnYqaO6OTww&_nc_ohc=bLIfJ2oeW00AX9yDBNz&_nc_ht=scontent.fdgo1-1.fna&oh=b645e34b0bf7380dc0970d83b0856353&oe=5EA490F5'),
(32, 'Carlos Gabriel', 'Ibarra', 'Martinez', 'fsFZkJWK7F', 'https://scontent.fdgo1-1.fna.fbcdn.net/v/t1.0-9/p720x720/82318853_10217716787685548_21839986009571328_o.jpg?_nc_cat=108&_nc_sid=85a577&_nc_eui2=AeETdmpD5H54_ULpUmDgNDb9brnhqCDlDHqbqlhF3UbXnNe_n1nXiQEKWLgZb_lvLbYQOusytyCa-b8TnjU2_vtwMvZHjt0kA5DefMLUX_WAoQ&_nc_ohc=PyWU-DehJD4AX-h-Wrn&_nc_ht=scontent.fdgo1-1.fna&_nc_tp=6&oh=3df9a0dfe61855b8f2045595925810c8&oe=5EA3777C'),
(33, 'David Alejandro', 'Rueda', 'Rivas', 'YZKCl6tqbC', 'https://scontent.fdgo1-1.fna.fbcdn.net/v/t1.0-9/s960x960/67508238_105670670784085_7409634547781861376_o.jpg?_nc_cat=107&_nc_sid=7aed08&_nc_eui2=AeEatybv7XFaFZGAU14RZWrttfoqIsEDQdjhcY_N8-yZHb5N9LolOslfVu7dn4dDuB22MRNDLj30AMG1PvbsPO_JD5bDVYwfQF5np9lEIzbzEQ&_nc_ohc=6V8qQaUZc28AX9iJ8ld&_nc_ht=scontent.fdgo1-1.fna&_nc_tp=7&oh=0bae6d02727cec95f5c5a651a9b9f3df&oe=5EA5C25F'),
(34, 'Florentino Octavio', 'Mejia', 'Rosales', 'kYdRpWCtvx', 'https://scontent.fdgo1-1.fna.fbcdn.net/v/t1.0-9/11223635_949800785061851_1323563003730662298_n.jpg?_nc_cat=107&_nc_sid=85a577&_nc_eui2=AeFAdOy4tu9cF-X7fMXWMFVA7C2a8zfvf5ji4ZYbzW4_pSnxX9jOkQKBiAin9ksxS38c_sq5SsDcjlIfBFDT5a8A0Gu7xsIbHLkIkexeP334TA&_nc_ohc=Np3Wz8fminwAX9FruH8&_nc_ht=scontent.fdgo1-1.fna&oh=b1b559f72a1941eddee1463d1fcf21f4&oe=5EA5882E'),
(35, 'Christian Antonio', 'Avila', 'Saucedo', 'DS6sm3wRnS', 'https://scontent.fdgo1-1.fna.fbcdn.net/v/t1.0-9/s960x960/60495913_394880707763875_3046197471559548928_o.jpg?_nc_cat=103&_nc_sid=85a577&_nc_eui2=AeHiArRi-mE3OeKM9F8ItXzK0ugCtlIj4_Y6j21TzEUZDGU-a5gKXLshWtjHU_AljFLEBx2BTbCMo5ppkZPEY9YVuoKOAF3cUBcPrYp2FdKgRg&_nc_ohc=qKrD2iH2Vr8AX8qMsaJ&_nc_ht=scontent.fdgo1-1.fna&_nc_tp=7&oh=a41eaf711766f62d05b67feb97626ca5&oe=5EA249BA'),
(36, 'Cesar Alberto', 'Schietekat', 'Castellanos', 'dilYGKfB0I', 'https://scontent.fdgo1-1.fna.fbcdn.net/v/t1.0-9/s960x960/65448785_2171460962979643_6572388878823456768_o.jpg?_nc_cat=111&_nc_sid=85a577&_nc_eui2=AeFK1RLXUGTDpn0JJJIOfD_oVSSUdn2d6a3exE_kgyXSRo54aBup-GEo9o_9Is11KqAXBz4hEvDVygqE-3C3rFNQozyCIS37OWiRvfcAC2iVog&_nc_ohc=vGhslBR-988AX95j-7b&_nc_ht=scontent.fdgo1-1.fna&_nc_tp=7&oh=10ad71071362a45a1d0a9e5dc89554af&oe=5EA27555'),
(37, 'Arely Guadalupe', 'Ibarra', 'Rivas', 'JXOaNI7UD0', 'https://scontent.fdgo1-1.fna.fbcdn.net/v/t1.0-9/s960x960/40875975_2145156415754014_1528153255157170176_o.jpg?_nc_cat=102&_nc_sid=110474&_nc_eui2=AeEkDsv99ffszkdfr_ihr4Pw20xt6trXj0AOEYPQDwe0dNF0hszTyDjY6nj2XBT-CqAprI_ivI8RmwsjTudKnfMTpjIO5VsIKkF8Tak4QW5Ciw&_nc_ohc=eRPh2mC3vSEAX85cZ3c&_nc_ht=scontent.fdgo1-1.fna&_nc_tp=7&oh=00ae9b2c153d95669bcc8f815c14b08d&oe=5EA39FC5');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `empleados`
--
ALTER TABLE `empleados`
  ADD PRIMARY KEY (`empleado_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `empleados`
--
ALTER TABLE `empleados`
  MODIFY `empleado_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
