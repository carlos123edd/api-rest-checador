const bodyparser = require("body-parser");
const express = require("express");
const morgan = require("morgan");
const app = express();

app.set("port", process.env.PORT || 3001);

app.use(morgan("dev"));

app.use((req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept, Authorization"
    );
    res.setHeader(
        "Access-Control-Allow-Methods",
        "GET, POST, PATCH, PUT, DELETE, OPTIONS"
    );
    next();
});

app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended:false}));

app.use('/api/empleados', require("./routes/empleados"));
app.use('/api/asistencias', require("./routes/asistencias"));

app.listen(app.get("port"),()=>{
    console.log("Puerto", app.get("port"));
});
