const express = require("express");
const router = express.Router();
const pool = require("../databse");

router.get('/', async (req, res)=>{
    const data = await pool.query('SELECT * FROM asistencias');
    res.send(data);
});

router.post('/', async (req, res)=>{
    const { empleado_id } = req.body;
    const data = {
        empleado_id
    };
    await pool.query('INSERT INTO asistencias SET ?', [data]);
    res.send(data);
    
});

router.get('/for/:param/:value', async(req, res)=>{
    const param = req.params.param;
    const value = req.params.value;
    var data;
    
    switch(param) {
        case("fecha_entrada"):
            data = await pool.query('SELECT * FROM asitencias WHERE fecha_entrada = "'+ value + '"');
            break;

        case("fecha_salida"):
            data = await pool.query('SELECT * FROM asistencias WHERE fecha_salida = "'+ value + '"');
            break;
        
        case("asistio"):
            data = await pool.query('SELECT * FROM asistencias WHERE asistio = "'+ value + '"');
            break;

        case("empleado_id"):
            data = await pool.query('SELECT * FROM asistencias WHERE empleado_id = "'+ value + '"');
            break;

    }
    res.send(data);
});

router.delete('/', async(req, res)=>{
    const {id} = req.body;
    await pool.query('DELETE FROM asistencias WHERE asistencia_id = ?', id);
    res.send("Eliminado");
});

module.exports = router;