const express = require("express");
const router = express.Router();
const pool = require("../databse");

router.get('/', async (req, res)=>{
    const data = await pool.query('SELECT * FROM empleados');
    res.send(data);
});

router.post('/', async (req, res)=>{
    function makeid(length) {
        var result           = '';
        var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for ( var i = 0; i < length; i++ ) {
           result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
     }
const { nombre, apellidoP, apellidoM, imagen } = req.body;
    const clave = makeid(10);
    const data = {
        nombre, apellidoP, apellidoM, clave, imagen
    };
    await pool.query('INSERT INTO empleados SET ?', [data]);
    res.send(data.clave);

});

router.put('/for/:value2/:param/:value', async(req, res)=>{
    const param = req.params.param;
    const value = req.params.value;
    const value2 = req.params.value2;
    var data;
    
    switch(param) {
        case("nombre"):
            data = await pool.query
            ('UPDATE empleados SET nombre = "'+ value + '" WHERE empleado_id = "'+ value2 + '"');
            break;

        case("apellidoP"):
            data = await pool.query
            ('UPDATE empleados SET apellidoP = "'+ value + '" WHERE empleado_id = "'+ value2 + '"');
            break;

        case("apellidoM"):
            data = await pool.query
            ('UPDATE empleados SET apellidoM = "'+ value + '" WHERE empleado_id = "'+ value2 + '"');
            break;

        case("imagen"):
            const {img}=req.body
            const simon={img}
            data = await pool.query
            ('UPDATE empleados SET imagen = "'+ [simon] + '" WHERE empleado_id = "'+ value2 + '"');
            break;

    }
    res.send(data);
});

router.get('/for/:param/:value', async(req, res)=>{
    const param = req.params.param;
    const value = req.params.value;
    var data;
    
    switch(param) {
        case("nombre"):
            data = await pool.query('SELECT * FROM empleados WHERE nombre = "'+ value + '"');
            break;

        case("apellidoP"):
            data = await pool.query('SELECT * FROM empleados WHERE apellidoP = "'+ value + '"');
            break;
        
        case("apellidoM"):
            data = await pool.query('SELECT * FROM empleados WHERE apellidoM = "'+ value + '"');
            break;

        case("clave"):
            data = await pool.query('SELECT * FROM empleados WHERE clave = "'+ value + '"');
            break;

        case("imagen"):
            data = await pool.query('SELECT * FROM empleados WHERE imagen = "'+ value + '"');
            break;

    }
    res.send(data);
});

router.delete('/for/:param/:value', async(req, res)=>{
    const param = req.params.param;
    const value = req.params.value;
    var data;
    switch(param){
        case("empleado_id"):
        data = await pool.query('DELETE FROM asistencias WHERE empleado_id = "'+ value + '"');
        data = await pool.query('DELETE FROM empleados WHERE empleado_id = "'+ value + '"');
        break;
    }
    res.send("  Eliminado");
});

module.exports = router;